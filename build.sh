#!/usr/bin/bash

echo updating submodules
git submodule init
git submodule update --remote
oldgopath=$GOPATH
echo setting new gopath
export GOPATH=$(realpath ./go)
cd go/src/gitlab.com/rytone/chroma-backbone
echo installing backbone deps
go get
echo building backbone
go install
cd ../../../../..
echo copying backbone to bin
cp go/bin/chroma-backbone bin/chroma-backbone
echo reverting gopath
export GOPATH=$oldgopath
echo building shard
cd shard
gradle build
cd ..
cp shard/build/libs/shard.jar bin/shard.jar
echo "done"
